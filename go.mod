module gitlab.com/go-yp/proto-vs-json-research

go 1.14

require (
	github.com/gogo/protobuf v1.3.1
	github.com/mailru/easyjson v0.7.1
	github.com/stretchr/testify v1.5.1
)
