package benchmarks

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-yp/proto-vs-json-research/models/protos"
	"testing"
)

func TestProtoRepositoryUnmarshalJSON(t *testing.T) {
	var repository = &protos.Repository{}
	var unmarshalErr = json.Unmarshal([]byte(jsonRepositoryFixture), &repository)

	require.NoError(t, unmarshalErr)
	require.Equal(t, protoExpectedRepository, repository)
}

func BenchmarkRepositoryFasterMarshalProto(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, _ = protoExpectedRepository.Marshal()
	}
}

func BenchmarkRepositoryFasterUnmarshalProto(b *testing.B) {
	var content, marshalErr = protoExpectedRepository.Marshal()
	require.NoError(b, marshalErr)

	{
		var repository = protos.Repository{}
		var unmarshalErr = repository.Unmarshal(content)

		require.NoError(b, unmarshalErr)

		require.Equal(b, protoExpectedRepository, &repository)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var repository = protos.Repository{}

		_ = repository.Unmarshal(content)
	}
}
