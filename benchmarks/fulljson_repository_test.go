package benchmarks

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-yp/proto-vs-json-research/models/fulljson"
	"testing"
)

func TestRepositoryUnmarshalJSON(t *testing.T) {
	var repository = fulljson.Repository{}

	var unmarshalErr = json.Unmarshal([]byte(jsonRepositoryFixture), &repository)

	require.NoError(t, unmarshalErr)
	require.Equal(t, jsonExpectedRepository, repository)
}

func TestRepositoryMarshalJSON(t *testing.T) {
	var resultJSON, marshalErr = json.Marshal(&jsonExpectedRepository)

	require.NoError(t, marshalErr)
	require.Equal(t, []byte(jsonRepositoryFixture), resultJSON)
}

func BenchmarkRepositoryEasyMarshalJSON(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, _ = jsonExpectedRepository.MarshalJSON()
	}
}

func BenchmarkRepositoryEasyUnmarshalJSON(b *testing.B) {
	var fixture = []byte(jsonRepositoryFixture)

	for i := 0; i < b.N; i++ {
		var repository = fulljson.Repository{}

		_ = repository.UnmarshalJSON(fixture)
	}
}
