package benchmarks

import (
	"gitlab.com/go-yp/proto-vs-json-research/models/protos"
)

var (
	protoExpectedRepository = &protos.Repository{
		Id:       23096959,
		NodeId:   "MDEwOlJlcG9zaXRvcnkyMzA5Njk1OQ==",
		Name:     "go",
		FullName: "golang/go",
		Private:  false,
		Owner: &protos.Owner{
			Login:             "golang",
			Id:                4314092,
			NodeId:            "MDEyOk9yZ2FuaXphdGlvbjQzMTQwOTI=",
			AvatarUrl:         "https://avatars3.githubusercontent.com/u/4314092?v=4",
			GravatarId:        "",
			Url:               "https://api.github.com/users/golang",
			HtmlUrl:           "https://github.com/golang",
			FollowersUrl:      "https://api.github.com/users/golang/followers",
			FollowingUrl:      "https://api.github.com/users/golang/following{/other_user}",
			GistsUrl:          "https://api.github.com/users/golang/gists{/gist_id}",
			StarredUrl:        "https://api.github.com/users/golang/starred{/owner}{/repo}",
			SubscriptionsUrl:  "https://api.github.com/users/golang/subscriptions",
			OrganizationsUrl:  "https://api.github.com/users/golang/orgs",
			ReposUrl:          "https://api.github.com/users/golang/repos",
			EventsUrl:         "https://api.github.com/users/golang/events{/privacy}",
			ReceivedEventsUrl: "https://api.github.com/users/golang/received_events",
			Type:              "Organization",
			SiteAdmin:         false,
		},
		HtmlUrl:          "https://github.com/golang/go",
		Description:      "The Go programming language",
		Fork:             false,
		Url:              "https://api.github.com/repos/golang/go",
		ForksUrl:         "https://api.github.com/repos/golang/go/forks",
		KeysUrl:          "https://api.github.com/repos/golang/go/keys{/key_id}",
		CollaboratorsUrl: "https://api.github.com/repos/golang/go/collaborators{/collaborator}",
		TeamsUrl:         "https://api.github.com/repos/golang/go/teams",
		HooksUrl:         "https://api.github.com/repos/golang/go/hooks",
		IssueEventsUrl:   "https://api.github.com/repos/golang/go/issues/events{/number}",
		EventsUrl:        "https://api.github.com/repos/golang/go/events",
		AssigneesUrl:     "https://api.github.com/repos/golang/go/assignees{/user}",
		BranchesUrl:      "https://api.github.com/repos/golang/go/branches{/branch}",
		TagsUrl:          "https://api.github.com/repos/golang/go/tags",
		BlobsUrl:         "https://api.github.com/repos/golang/go/git/blobs{/sha}",
		GitTagsUrl:       "https://api.github.com/repos/golang/go/git/tags{/sha}",
		GitRefsUrl:       "https://api.github.com/repos/golang/go/git/refs{/sha}",
		TreesUrl:         "https://api.github.com/repos/golang/go/git/trees{/sha}",
		StatusesUrl:      "https://api.github.com/repos/golang/go/statuses/{sha}",
		LanguagesUrl:     "https://api.github.com/repos/golang/go/languages",
		StargazersUrl:    "https://api.github.com/repos/golang/go/stargazers",
		ContributorsUrl:  "https://api.github.com/repos/golang/go/contributors",
		SubscribersUrl:   "https://api.github.com/repos/golang/go/subscribers",
		SubscriptionUrl:  "https://api.github.com/repos/golang/go/subscription",
		CommitsUrl:       "https://api.github.com/repos/golang/go/commits{/sha}",
		GitCommitsUrl:    "https://api.github.com/repos/golang/go/git/commits{/sha}",
		CommentsUrl:      "https://api.github.com/repos/golang/go/comments{/number}",
		IssueCommentUrl:  "https://api.github.com/repos/golang/go/issues/comments{/number}",
		ContentsUrl:      "https://api.github.com/repos/golang/go/contents/{+path}",
		CompareUrl:       "https://api.github.com/repos/golang/go/compare/{base}...{head}",
		MergesUrl:        "https://api.github.com/repos/golang/go/merges",
		ArchiveUrl:       "https://api.github.com/repos/golang/go/{archive_format}{/ref}",
		DownloadsUrl:     "https://api.github.com/repos/golang/go/downloads",
		IssuesUrl:        "https://api.github.com/repos/golang/go/issues{/number}",
		PullsUrl:         "https://api.github.com/repos/golang/go/pulls{/number}",
		MilestonesUrl:    "https://api.github.com/repos/golang/go/milestones{/number}",
		NotificationsUrl: "https://api.github.com/repos/golang/go/notifications{?since,all,participating}",
		LabelsUrl:        "https://api.github.com/repos/golang/go/labels{/name}",
		ReleasesUrl:      "https://api.github.com/repos/golang/go/releases{/id}",
		DeploymentsUrl:   "https://api.github.com/repos/golang/go/deployments",
		CreatedAt:        "2014-08-19T04:33:40Z",
		UpdatedAt:        "2020-04-13T20:05:24Z",
		PushedAt:         "2020-04-13T20:05:59Z",
		GitUrl:           "git://github.com/golang/go.git",
		SshUrl:           "git@github.com:golang/go.git",
		CloneUrl:         "https://github.com/golang/go.git",
		SvnUrl:           "https://github.com/golang/go",
		Homepage:         "https://golang.org",
		Size_:            209484,
		StargazersCount:  71178,
		WatchersCount:    71178,
		Language:         "Go",
		HasIssues:        true,
		HasProjects:      true,
		HasDownloads:     true,
		HasWiki:          true,
		HasPages:         false,
		ForksCount:       10164,
		MirrorUrl:        "",
		Archived:         false,
		Disabled:         false,
		OpenIssuesCount:  5717,
		License: &protos.License{
			Key:    "other",
			Name:   "Other",
			SpdxId: "NOASSERTION",
			Url:    "",
			NodeId: "MDc6TGljZW5zZTA=",
		},
		Forks:          10164,
		OpenIssues:     5717,
		Watchers:       71178,
		DefaultBranch:  "master",
		TempCloneToken: "",
		Organization: &protos.Organization{
			Login:      "golang",
			Id:         4314092,
			NodeId:     "MDEyOk9yZ2FuaXphdGlvbjQzMTQwOTI=",
			AvatarUrl:  "https://avatars3.githubusercontent.com/u/4314092?v=4",
			GravatarId: "", Url: "https://api.github.com/users/golang",
			HtmlUrl:           "https://github.com/golang",
			FollowersUrl:      "https://api.github.com/users/golang/followers",
			FollowingUrl:      "https://api.github.com/users/golang/following{/other_user}",
			GistsUrl:          "https://api.github.com/users/golang/gists{/gist_id}",
			StarredUrl:        "https://api.github.com/users/golang/starred{/owner}{/repo}",
			SubscriptionsUrl:  "https://api.github.com/users/golang/subscriptions",
			OrganizationsUrl:  "https://api.github.com/users/golang/orgs",
			ReposUrl:          "https://api.github.com/users/golang/repos",
			EventsUrl:         "https://api.github.com/users/golang/events{/privacy}",
			ReceivedEventsUrl: "https://api.github.com/users/golang/received_events",
			Type:              "Organization",
			SiteAdmin:         false,
		},
		NetworkCount:     10164,
		SubscribersCount: 3448,
	}
)
