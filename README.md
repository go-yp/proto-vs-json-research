# Proto vs json research

### Benchmark

![](images/proto-vs-json-total.png)

| name                                    | time/op     | alloc/op  | allocs/op     |
|-----------------------------------------|-------------|-----------|---------------|
| BenchmarkRepositoryMarshalJSON          | 13172 ns/op | 6146 B/op | 1 allocs/op   |
| BenchmarkRepositoryUnmarshalJSON        | 51246 ns/op | 6256 B/op | 105 allocs/op |
| BenchmarkRepositoryMarshalProto         | 8302 ns/op  | 4208 B/op | 8 allocs/op   |
| BenchmarkRepositoryUnmarshalProto       | 9357 ns/op  | 5968 B/op | 94 allocs/op  |
| BenchmarkRepositoryEasyMarshalJSON      | 9718 ns/op  | 6867 B/op | 8 allocs/op   |
| BenchmarkRepositoryEasyUnmarshalJSON    | 13996 ns/op | 4128 B/op | 86 allocs/op  |
| BenchmarkRepositoryFasterMarshalProto   | 1705 ns/op  | 4096 B/op | 1 allocs/op   |
| BenchmarkRepositoryFasterUnmarshalProto | 3894 ns/op  | 4784 B/op | 89 allocs/op  |


### Benchstat [#](https://godoc.org/golang.org/x/perf/cmd/benchstat)
```bash
go test ./benchmarks/... -v -run=$^ -bench=. -benchmem -count=10 > bench.text
benchstat bench.text
```

| name                           | time/op     |
|--------------------------------|-------------|
| RepositoryMarshalJSON          | 14.2µs ± 7% |
| RepositoryUnmarshalJSON        | 52.2µs ± 3% |
| RepositoryEasyMarshalJSON      | 11.5µs ±16% |
| RepositoryEasyUnmarshalJSON    | 14.7µs ± 3% |
| RepositoryMarshalProto         | 8.52µs ± 3% |
| RepositoryUnmarshalProto       | 10.0µs ±10% |
| RepositoryFasterMarshalProto   | 1.78µs ± 6% |
| RepositoryFasterUnmarshalProto | 5.43µs ±41% |

| name                           | alloc/op    |
|--------------------------------|-------------|
| RepositoryMarshalJSON          | 6.15kB ± 0% |
| RepositoryUnmarshalJSON        | 6.26kB ± 0% |
| RepositoryEasyMarshalJSON      | 6.87kB ± 0% |
| RepositoryEasyUnmarshalJSON    | 4.13kB ± 0% |
| RepositoryMarshalProto         | 4.21kB ± 0% |
| RepositoryUnmarshalProto       | 5.97kB ± 0% |
| RepositoryFasterMarshalProto   | 4.10kB ± 0% |
| RepositoryFasterUnmarshalProto | 4.78kB ± 0% |

| name                           | allocs/op   |
|--------------------------------|-------------|
| RepositoryMarshalJSON          |   1.00 ± 0% |
| RepositoryUnmarshalJSON        |    105 ± 0% |
| RepositoryEasyMarshalJSON      |   8.00 ± 0% |
| RepositoryEasyUnmarshalJSON    |   86.0 ± 0% |
| RepositoryMarshalProto         |   8.00 ± 0% |
| RepositoryUnmarshalProto       |   94.0 ± 0% |
| RepositoryFasterMarshalProto   |   1.00 ± 0% |
| RepositoryFasterUnmarshalProto |   89.0 ± 0% |

##### Result:

| Format   | Bytes |
|----------|-------|
| JSON     | 5488  |
| Protobuf | 3811  |
