gogofaster:
	go get github.com/gogo/protobuf/protoc-gen-gogofaster

proto:
	protoc -I . protos/*.proto --gogofaster_out=models

easyjson:
	easyjson ./models/fulljson/repository.go

test:
	go test ./benchmarks/... -v -bench=. -benchmem